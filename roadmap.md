- Cuando se crea una compañia:
  - Para cada request en tipo X metodo:
    - Generar request
    - Encolar request

- Cola de revisión:
  - Para cada request pendiente:
    - Revisar el estado
    - Si está lista:
      - Obtener lista de paquetes
      - Para cada paquete:
        - Encolar descarga

- Cola de descarga:
  - Para cada request lista:
    - Para cada paquete en la request (paralelo)
      - Descargar el .zip (en S3)
      - Encolar procesamiento (incluir la compañia)

- Cola de procesamiento:
  - Para cada paquete descargado:
    - Descomprimir .zip {Actualmente en RAM}
      - Vaciar a S3, y elminar en RAM
    - *Obtener lista de CFDI's (yield) {Actualmente están TODOS al mismo tiempo en RAM}*
    - Para cada CFDI en los archivos descargados:
      - Si tiene contraparte en BD:
        - Completar información del existente (sin commit)
      - Si no:
        - Cargar nuevo CFDI a la BD (sin commit)
    - Eliminar .zip
