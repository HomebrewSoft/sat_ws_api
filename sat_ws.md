# Roadmap
1. Realizar peticiones
2. Verificar estado
3. Si está lista, descargar los paquetes


```bash
sat-ws $CERT $KEY $PASS request --ALL $START_END $END_DATE
UUID1
UUID2
UUID3
UUID4
```

```python
sat_ws.wait(UUID1, UUID2)
```

```bash
sat-ws $CERT $KEY $PASS verify UUID1 UUID2 UUID3 UUID4
UUID1: Ok [p1, p2, p3]
UUID2: Pending
UUID3: Ok [p1]
UUID4: Ok [p1, p2]
```

```bash
sat-ws $CERT $KEY $PASS download -p p1, p2, p3 -r UUID2
p1.zip
p2.zip
p3.zip
uuid2_p1.zip
uuid2_p2.zip
uuid2_p3.zip
uuid2_p4.zip
```
