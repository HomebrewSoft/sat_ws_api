import logging
import pickle
import random
import sys
from datetime import datetime, timedelta
from sys import getsizeof
from time import sleep
from typing import List

from mx_edi.connectors.sat import utils
from mx_edi.connectors.sat.enums import DownloadType, RequestType
from mx_edi.connectors.sat.package import Package
from mx_edi.connectors.sat.query import Query
from mx_edi.connectors.sat.sat_connector import SATConnector
from mx_edi.core import CFDI
from mx_edi.processors.static import StaticProcessor

_logger = logging.getLogger(__name__)


def get_connector(cert_path: str, pkey_path: str, password: bytes) -> SATConnector:
    with open(cert_path, "rb") as f:
        cert = f.read()
        with open(pkey_path, "rb") as f:
            pkey = f.read()
        return SATConnector(cert, pkey, password)


def get_params():
    cert_path = "efirmas/plataforma.cer"
    pkey_path = "efirmas/plataforma.key"
    password = b"pxD1009214w0" # sys.argv[3].encode("UTF-8")
    start = datetime.fromisoformat("2022-06-07 00:00:00") - timedelta(milliseconds=random.randint(0, 1000))
    # end = datetime.fromisoformat(sys.argv[5]) + timedelta(seconds=random.randint(0, 10000))
    end = datetime.fromisoformat("2022-06-07 23:59:59") + timedelta(milliseconds=random.randint(0, 1000))
    return cert_path, pkey_path, password, start, end


def get_start():
    return datetime.now() - timedelta(days=365 * 2)

issued_date = (
    datetime.fromisoformat("2021-01-01 00:00:00"),
    datetime.fromisoformat("2024-07-11 23:59:59")
)

def get_packages(connector: SATConnector, start, end):
    queries = (
        Query(DownloadType.RECEIVED, RequestType.METADATA, start=datetime.fromisoformat("2021-01-01"), end=datetime.now()),
        Query(DownloadType.RECEIVED, RequestType.CFDI, start=datetime.fromisoformat("2021-01-01"), end=datetime.now()),
        Query(DownloadType.ISSUED, RequestType.METADATA, start=datetime.fromisoformat("2021-01-01"), end=datetime.now()),
        Query(DownloadType.ISSUED, RequestType.CFDI, start=datetime.fromisoformat("2021-01-01"), end=datetime.now()),
        # Query(identifier='a8f1c880-ac2c-4f9f-8882-89b5b3b41f8c'),
    )
    for query in queries:
        if not query.identifier:
            query.send(connector)
            query.query_status = None

    all_done = False
    need_sleep = True
    while not all_done:
        all_done = True
        print(f"Checking {datetime.now()}")
        for query in queries:
            # if not query.query_status or query.query_status != 3:
            query.verify(connector)
            # print(f"{query.download_type}-{query.request_type}-{query.identifier} - {query.query_status}")
            print(query)
            if query.query_status != 3:
                all_done = False
        if need_sleep and not all_done:
            sleep(300)
    pass


def get_cfdis(packages: List[Package]):

    cfdis = []
    _logger.info("Getting CFDIs from packages")
    for package in packages:
        _logger.info("Getting CFDIs from package %s", package.identifier)
        package._process_download_response()
    for package in packages:
        cfdis.extend(package.cfdis)

    with open("cfdis.chk", "wb") as checkpoint:
        pickle.dump(cfdis, checkpoint)
    return cfdis


def main():
    cert_path, pkey_path, password, start, end = get_params()

    connector = get_connector(cert_path, pkey_path, password)
    packages = get_packages(connector, start, end)

if __name__ == "__main__":
    main()
