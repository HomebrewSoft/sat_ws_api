# TO Do List

## WIP

- [x] Unit Test
- [ ] Documentation
- [x] Custom Exceptions

  # VerificaSolicitudDescarga

  - 300: Usuario No Válido
  - 301: XML Mal Formado
    - Este  código de error se regresa cuando el request posee información invalida, ejemplo: un RFC de receptor no válido.
  - 302: Sello Mal Formado
  - 303: Sello no corresponde con Rfc Solicitante
  - 304: Certificado Revocado o Caduco
    - El certificado fue revocado o bien la fecha de vigencia expiró.
  - 305: Certificado Inválido
    - El certificado puede ser invalido por múltiples razones como son el tipo, codificación incorrecta, etc.
  - 5000: Solicitud recibida con éxito
    - La petición de verificación fue recibida con éxito.
  - 5004: No se encontró la información
    - No se encontró la solicitud con el identificador enviado.

  # Solicitud Descarga Masiva

  - 5000: Solicitud recibida con éxito
    - Indica que la solicitud de descarga que se está verificando fue aceptada.
  - 5002: Se agotó las solicitudes de por vida
    - Para el caso de descarga de tipo CFDI, se tiene un límite máximo para solicitudes con los mismos parámetros (Fecha Inicial, Fecha Final, RfcEmisor, RfcReceptor).
  - 5003: Tope máximo
    - Indica que en base a los parámetros de consulta se está superando el tope máximo de CFDI o Metadata, por solicitud de descarga masiva.
  - 5004: No se encontró la información
    - Indica que la solicitud de descarga que se está verificando no generó paquetes por falta de información.
  - 5005: Solicitud duplicada
    - En caso de que exista una solicitud vigente con los mismos parámetros (Fecha Inicial, Fecha Final, RfcEmisor, RfcReceptor, TipoSolicitud), no se permitirá generar una nueva solicitud.
  - 404: Error no Controlado
    - Error genérico, en caso de presentarse realizar nuevamente la petición y si persiste el error levantar un RMA.

  # EstadoSolicitud

  - 1: Aceptada
  - 2: EnProceso
  - 3: Terminada
  - 4: Error
  - 5: Rechazada
  - 6: Vencida

## PENDING

- [ ] Handle Multiple packages
- [ ] Standardize all integer responses
- [ ] Badges
- [ ] CHANGELOG
- [ ] CONTRIBUTING
- [ ] LICENSE
