setup-virtualenv:
	python3 -m pip install virtualenv
	virtualenv venv
	. venv/bin/activate # BUG Not work

install-dev-deps: setup-virtualenv
	pip install --upgrade pip
	pip install -r requirements.txt

install: setup-virtualenv
	pip install -r mx_edi/requirements.txt

prepare-dev-env: install-dev-deps install
	pre-commit install

lint:
	pylint mx_edi
	pylint tests

typehint:
	mypy --ignore-missing-imports mx_edi/
	mypy --ignore-missing-imports tests/

test:
	pytest -cov=mx_edi tests

coverage-html: test
	coverage html

lint-format:
	black -l 100 --check .

review: lint-format lint typehint

format:
	isort .
	black -l 100 .

clean: check
	find mx_edi -type f -name "*.pyc" -delete
	find tests -type f -name "*.pyc" -delete
	find mx_edi -type d -name "__pycache__" -delete
	find tests -type d -name "__pycache__" -delete
	rm -rf .mypy_cache
	rm -rf .pytest_cache

check:
	@echo -n "Are you sure? [y/N] " && read ans && [ $${ans:-N} = y ]

create_wheel:
	python setup.py bdist_wheel

publish:
	twine upload dist/*
