import concurrent.futures
import logging
import os
from datetime import datetime, timedelta
from time import sleep

from mx_edi.connectors.sat.enums import DownloadType, RequestType
from mx_edi.connectors.sat.query import Query
from mx_edi.connectors.sat.sat_connector import SATConnector

_logger = logging.getLogger(__name__)

LOG_PATH = os.environ.get("SAT_WS_TESTER_LOG_PATH", "log.txt")
CERT_PATH = os.environ["SAT_WS_TESTER_CERT_PATH"]
PKEY_PATH = os.environ["SAT_WS_TESTER_PKEY_PATH"]
PASSWORD = os.environ["SAT_WS_TESTER_PASSWORD"].encode("UTF-8")

LOOPS_PER_HOUR = 6
LOOPS_TO_SEND_NEW_QUERIES = 6

NUM_CLONES_BY_VARIANT = 5

LOOP_SLEEP_SECONDS = 60 * 60 / LOOPS_PER_HOUR

TIME_WINDOWS: list[timedelta] = [
    timedelta(days=1),
    timedelta(days=7),
    timedelta(days=30),
    timedelta(days=365),
    timedelta(days=365 * 5),
]

AGE_LIMITS: list[timedelta] = [
    timedelta(days=1),
    timedelta(days=7),
    timedelta(days=30),
    timedelta(days=365),
    timedelta(days=365 * 5),
]

DOWNLOAD_TYPES: list[DownloadType] = [
    DownloadType.ISSUED,
    DownloadType.RECEIVED,
]

REQUEST_TYPES: list[RequestType] = [
    RequestType.CFDI,
    RequestType.METADATA,
]

# Comuputed values


def generate_variants():
    """
    Generate all possible variants of the parameters.
    """
    for _i in range(NUM_CLONES_BY_VARIANT):
        for time_window in TIME_WINDOWS:
            for age_limit in AGE_LIMITS:
                for download_type in DOWNLOAD_TYPES:
                    for request_type in REQUEST_TYPES:
                        yield (time_window, age_limit, download_type, request_type)


def is_creation_loop(current_loop: int):
    return current_loop % LOOPS_TO_SEND_NEW_QUERIES == 0


def async_verify(query: Query, connector: SATConnector):
    query.verify(connector)
    _logger.info("Checked query %s", query.identifier)
    return query


def done(query: Query):
    detail = dict(
        time_to_verify=query.verified_date - query.sent_date,
        download_type=query.download_type,
        request_type=query.request_type,
        start=query.start,
        end=query.end,
        cfdi_qty=query.cfdi_qty,
        window=query.end - query.start,
    )
    _logger.info("Query %s done, Detal: %s", query.identifier, detail)


def check_queries(queries: dict[str, Query], connector: SATConnector):
    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
        futures = {
            executor.submit(async_verify, query, connector): query for _id, query in queries.items()
        }
    for future in futures:
        query = future.result()
        if query.query_status == 3:
            done(query)
            del queries[query.identifier]
    _logger.info("Queries remaining: %s", len(queries))


def async_send(query: Query, connector: SATConnector):
    query.send(connector)
    _logger.info("Sent query %s", query.identifier)
    return query


def create_and_send_new_queries(connector: SATConnector) -> dict[str, Query]:
    _logger.debug("Creating new queries")
    futures = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
        for variant in generate_variants():
            time_window, age_limit, download_type, request_type = variant
            now = datetime.now()  # Use in this loop to create micro-variants in each query
            end = now - age_limit
            start = end - time_window
            query = Query(
                download_type=download_type,
                request_type=request_type,
                start=start,
                end=end,
            )
            futures[executor.submit(async_send, query, connector)] = query
    queries = {}
    for future in futures:
        query = future.result()
        if query.identifier in queries:
            _logger.warning("Duplicated identifier %s", query.identifier)
        queries[query.identifier] = query
    _logger.info("Created %s queries", len(queries))
    return queries


def prepare_logging():
    handler = logging.FileHandler(LOG_PATH)
    _logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    _logger.addHandler(handler)


def get_sat_connector() -> SATConnector:
    with open(CERT_PATH, "rb") as cert_file:
        cert = cert_file.read()
        with open(PKEY_PATH, "rb") as key_file:
            pkey = key_file.read()
        return SATConnector(cert, pkey, PASSWORD)


def main():
    prepare_logging()

    current_loop = 0
    queries_pending: dict[str, Query] = {}
    connector = get_sat_connector()
    while True:
        check_queries(queries_pending, connector)
        if is_creation_loop(current_loop):
            new_queries = create_and_send_new_queries(connector)
            queries_pending.update(new_queries)
        current_loop += 1
        sleep(LOOP_SLEEP_SECONDS)


if __name__ == "__main__":
    main()
